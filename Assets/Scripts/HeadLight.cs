﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadLight : MonoBehaviour {

    private float rot;
    public float speed;

    // Use this for initialization
    void Start () {
        rot = 0;
	}
	
	// Update is called once per frame
	void Update () {
        rot += speed * Time.deltaTime;
        
        transform.localRotation = Quaternion.Euler(10f, rot, 0);
    }
}
