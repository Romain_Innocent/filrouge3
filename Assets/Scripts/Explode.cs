﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explode : MonoBehaviour {
	
	public GameObject boom;

	// Use this for initialization
	void Start () {
		boom.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Player") {
			boom.SetActive(true);
			Invoke("des", 1);
		}
	}

	void des()
	{
		Destroy(gameObject);
	}
		
}
