﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStat : MonoBehaviour
{

    public float health;
    private bool isArrived;
    private bool canMove;
    public float maxSpeed;
    public float minSpeed;
    private float speed;
    private bool hidden;

    public int startCrew;
    private int crew;

    public void Start()
    {
        hidden = false;
        canMove = true;
        isArrived = false;
        crew = startCrew;
        setSpeed();
    }

    public float getHealth() {
        return health;
    }

    public void modifyHealth(float value) {
        health += value;
        health = Mathf.Clamp(health, 0, 300);
    }

    public void HasArrived() {
        isArrived = true;
    }

    public bool getIsArrived() {
        return isArrived;
    }

    public void StopPlayer(){
        canMove = false;
    }

    public bool CanMove() {
        return canMove;
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed() {
        if (hidden)
            speed = minSpeed;
        else
            speed = maxSpeed;
    }

    public void HideShow() {
        hidden = !hidden;
        setSpeed();
    }

    public bool isHidden() {
        return hidden;
    }

    public void addCrew(int nbr) {
        crew += nbr;
    }

    public int getCrew() {
        return crew;
    }
}
