﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarScript : MonoBehaviour {

    [SerializeField]
    private Image value_bar;

    private PlayerStat playerScript;

    public GameObject player;
    public float maxHealth;

	// Use this for initialization
	void Start () {
        playerScript = player.GetComponent<PlayerStat>();
	}
	
	// Update is called once per frame
	void Update () {
        HandleBar(GetHealth(playerScript.health, 0, maxHealth, 0, 1));
	}

    private void HandleBar(float life) {
        value_bar.fillAmount = life;
    }

    private float GetHealth(float value, float min, float max, float min_ratio, float max_ratio) {

        return (value - min) * (max_ratio - min_ratio) / (max - min) + min_ratio;
    }
}
