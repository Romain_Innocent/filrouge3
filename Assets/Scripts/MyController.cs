﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MyController : MonoBehaviour
{

    public Transform canvas;
    public float timer;
    public float maxTimer;
    private PlayerStat playerScript;
    public GameObject player;
    private bool gameFinished;
    private bool won;
    public GUIText restatText;
    public GUIText timerText;
    public AudioSource defeat_sound;
    public AudioSource victory_sound;
    public AudioSource wood_crack_sound;


    // Use this for initialization
    void Start()
    {
        timer = 0;
        playerScript = player.GetComponent<PlayerStat>();
        gameFinished = false;
        won = false;
        restatText.text = "";
    }


    void isPaused()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            Paused();
        }
    }

    public void Paused()
    {
        if (canvas.gameObject.activeInHierarchy == false)
        {
            canvas.gameObject.SetActive(true);
            Time.timeScale = 0;
        }
        else
        {
            canvas.gameObject.SetActive(false);
            Time.timeScale = 1;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!gameFinished)
        {
            timer = Time.timeSinceLevelLoad;
            IsGameOver();
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.Return)) {
                Restart();
            }
        }

        isPaused();

        setTimer();

		goMenu ();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "FinishLine")
        {
            print("FROM MYCONTROLLER BOAT JUST ARRIVED");
            won = true;
            gameFinished = true;
        }
    }

    
	void goMenu()
	{
		if (Input.GetKeyDown (KeyCode.Escape)) {
			SceneManager.LoadScene ("Menu");
		}
	}

    void IsGameOver()
    {
        if (Input.GetKeyDown(KeyCode.R) || playerScript.getHealth() <= 0 || timer > maxTimer)
        {
            if (playerScript.getHealth() <= 0)
                wood_crack_sound.Play();
            lostGame();
        }
        if (playerScript.getIsArrived())
        {
            wonGame();
        }
    }

    private void lostGame() {
        restatText.text = "You lost ! Press ENTER to restart the game.";
        playerScript.StopPlayer();
        gameFinished = true;
        defeat_sound.Play();
    }

    private void wonGame()
    {
        restatText.text = "You won with a timer of " + timer + " ! Press ENTER to restart the game.";
        playerScript.StopPlayer();
        won = true;
        gameFinished = true;
        victory_sound.Play();
    }

    public void Restart()
    {
        Application.LoadLevel(Application.loadedLevel);
    }

    private void setTimer() {
        timerText.text = "" + (int)timer + " / " + maxTimer;
    }
}