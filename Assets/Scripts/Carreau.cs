﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Carreau : MonoBehaviour {


    public float speed;
    public float damage;
    bool fired;
    
    PlayerStat player;
    GameObject targetPlayer;
    Vector3 direction;

    private GameObject sounds;
    private AudioSource collisionSound;


    // Use this for initialization
    void Start () {
        sounds = GameObject.Find("ShootSound");
        collisionSound = sounds.GetComponent<AudioSource>();

    }
	
	// Update is called once per frame
	void Update () {
        if (fired) {
            transform.position += direction * speed * Time.deltaTime;
        }
	}

    public void Fire(GameObject barrel, GameObject target) {
        player = target.GetComponent<PlayerStat>();
        targetPlayer = target;
        direction = (target.transform.position - barrel.transform.position).normalized;
        fired = true;

        Destroy(gameObject, 10f);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject == targetPlayer)
        {
            collisionSound.Play();
            player.modifyHealth(-damage);
        }
        Destroy(gameObject);
    }
}
