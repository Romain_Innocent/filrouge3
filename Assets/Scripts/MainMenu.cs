﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

    public int var = 0;

	public
	// Use this for initialization
	void Start () {
		
	}


    public void ChangeLevel(string gameLevel)
    {
        SceneManager.LoadScene(gameLevel);
        GameObject cont = GameObject.FindGameObjectWithTag("Controller");
        if (var != 0)
        { 
            MyController cb = cont.GetComponent<MyController>();
            cb.Restart();
        }
        var++;
    }

	// Update is called once per frame
	void Update () {
		
	}
}
