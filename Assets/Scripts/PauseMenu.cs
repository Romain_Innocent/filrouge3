﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class PauseMenu : MonoBehaviour {

	public void changeMenu(string gameMenu)
	{
		SceneManager.LoadScene (gameMenu);
        Time.timeScale = 1;
    }

	public void Resume()
	{
		GameObject cont = GameObject.FindGameObjectWithTag("Controller");
		MyController cb = cont.GetComponent<MyController>();
		cb.Paused();
		Console.WriteLine ("res");
	}
}
