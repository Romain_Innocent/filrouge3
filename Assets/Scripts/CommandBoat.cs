﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommandBoat : MonoBehaviour {
    
    private PlayerStat player;
    private GameObject hideSail1;
    private GameObject hideSail2;
    private GameObject hideSail3;

    private Rigidbody rb;
    public AudioSource bump;

    private float repairTimer;
    public float turnSpeed;

    public GUIText crewText;

    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody>();
        player = GetComponent<PlayerStat>();
        hideSail1 = GameObject.Find("HideSail1");
        hideSail2 = GameObject.Find("HideSail2");
        hideSail3 = GameObject.Find("HideSail3");
        repairTimer = 0;
    }
	
	// Update is called once per frame
	void Update () {
        
        float hDir = Input.GetAxis("Horizontal");
        float vDir = Input.GetAxis("Vertical");

        if (player.CanMove())
        {
            rb.AddTorque(0.0f, hDir * player.getCrew() * turnSpeed * Time.deltaTime, 0.0f);
            rb.AddForce(transform.forward * vDir * player.getSpeed() * Time.deltaTime);
        }
        if (Input.GetKeyDown(KeyCode.Q)) {
            HideShowSail();
        }

        crewText.text = player.getCrew() + " crew members";

        repairBoat();

    }

    private void OnCollisionEnter(Collision collision)
    {
        float speed = rb.velocity.magnitude;
        if (speed > 10)
        {
            bump.Play();
            player.modifyHealth(-speed);
            print("YOUR SHIP SANK : " + speed);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "FinishLine") {
            print("BOAT JUST ARRIVED");
            player.HasArrived();
        }

		if (other.tag == "Health") {
			print("HEALTH RECOVERED");
			player.modifyHealth(100);
			Destroy (other.gameObject);
		}

		if (other.tag == "Bomb") {
			print("BOOM");
			player.modifyHealth(-150);
		
		}

        if (other.tag == "sailor")
        {
            player.addCrew(1);
            Destroy(other.gameObject);
        }
    }

    private void repairBoat() {
        repairTimer += Time.deltaTime;

        if (repairTimer > 2) {
            repairTimer = 0;
            player.modifyHealth(player.getCrew());
        }
    }

    private void HideShowSail()
    {
        player.HideShow();
        if (player.isHidden())
        {
            hideSail1.SetActive(false);
            hideSail2.SetActive(false);
            hideSail3.SetActive(false);
        }
        else
        {
            hideSail1.SetActive(true);
            hideSail2.SetActive(true);
            hideSail3.SetActive(true);
        }
    }
}
