﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThunderLight : MonoBehaviour {

    private float timer;
    public float timeActiveMax;
    private float timeActive;
    private Light light;
    private bool cycleActive;
    private float randomInt;
    public float minWait;
    public float maxWait;
    public AudioSource sound_1;
    public AudioSource sound_2;
    public AudioSource sound_3;
    int soundId;

    // Use this for initialization
    void Start() {
        timer = 0;
        light = GetComponent<Light>();
        timeActive = 0;
        cycleActive = false;
        WaitTimer();
    }

    // Update is called once per frame
    void Update() {
        timer += Time.deltaTime;
        
        if (timer >= randomInt && !cycleActive) {
            soundId = Random.Range(1, 4);
            print("THUNDER : " + soundId);
            Thunder();
        }
        if (cycleActive)
            Thundering();
    }

    private void Thunder() {
        light.enabled = true;
        cycleActive = true;
        if (soundId == 1)
            sound_1.Play();
        if (soundId == 2)
            sound_2.Play();
        if (soundId == 3)
            sound_3.Play();
    }

    private void Thundering() {
        if (timeActive <= timeActiveMax)
        {
            //print("Thundering");
            timeActive += Time.deltaTime;
        }
        else {
            print("Thundering OVER");
            timeActive = 0;
            timer = 0;
            light.enabled = false;
            cycleActive = false;
            WaitTimer();
        }
    }

    private void WaitTimer() {
        randomInt = Random.Range(minWait, maxWait);
    }
}
