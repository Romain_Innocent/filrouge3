﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tracking : MonoBehaviour {

    public float speed;
    public float range;

    //Position and target
    public GameObject target;
    public GameObject barrel;
    Vector3 lastPostition;
    Quaternion lookRotation;
    float distance;

    //FireRate
    public float fireRate;
    private float fireTimer;
    public float fieldView;
    public GameObject projectile;

    // Use this for initialization
    void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        Tracker();
        Shooting();
        distance = Vector3.Distance(transform.position, target.transform.position);
    }

    private void Tracker() {
        

        if (distance <= range)
        {
            if (lastPostition != target.transform.position)
            {
                lastPostition = target.transform.position;
                lookRotation = Quaternion.LookRotation(lastPostition - transform.position);
            }
            if (transform.rotation != lookRotation)
            {
                transform.rotation = Quaternion.RotateTowards(transform.rotation, lookRotation, speed * Time.deltaTime);
            }
        }
    }

    private void Shooting() {
        fireTimer += Time.deltaTime;

        if (fireTimer >= fireRate) {
            if (distance <= range) {
                float angle = Quaternion.Angle(transform.rotation, Quaternion.LookRotation(target.transform.position - transform.position));
                if (angle <= fieldView) {
                    Fire();
                    fireTimer = 0;
                }
            }
        }
    }

    private void Fire() {
        GameObject proj = Instantiate(projectile, barrel.transform.position, Quaternion.Euler(barrel.transform.forward)) as GameObject;
        proj.GetComponent<Carreau>().Fire(barrel, target);
    }
}
